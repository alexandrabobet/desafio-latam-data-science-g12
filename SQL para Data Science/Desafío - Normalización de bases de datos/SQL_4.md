## Tabla original

| codigo\_producto | producto | local           | precio | existencia | stock | ubicacion    | numero\_bodega | vendedor             | rut\_vendedor | numero\_boleta | cantidad\_vendida | rut\_cliente | nombre\_cliente |
|------------------|----------|-----------------|--------|------------|-------|--------------|----------------|----------------------|---------------|----------------|-------------------|--------------|-----------------|
| 43               | te       | Las Rosas       | 2000   | TRUE       | 30    | Providencia  | 21             | "Juan Pérez, Rosita" | 33333333      | 534            | 1                 | 77777777     | Fernando        |
| 25               | cafe     | Los Pinos       | 1500   | Si         | 40    | Los Leones   | 35             | "Jorge, Luisita"     | 444444444     | 75643          | 1                 | 3332323      | Cristian        |
| 6                | leche    | Los Estudiantes | 900    | 1          | 10    | Viña del Mar | 40             | Don Cristian         | 666666666     | 324            | 1                 | 56434513     | Angela          |
| 64               | queso    | Las Rosas       | 2000   | TRUE       | 70    | Providencia  | 62             | "Juan Pérez, Rosita" | 33333333      | 537            | 1                 | 123267845    | Iván            |
| 25               | cafe     | Los Pinos       | 1500   | Si         | 40    | Los Leones   | 35             | "Jorge, Luisita"     | 444444444     | 7569           | 1                 | 4537145      | Felipe          |
| 6                | leche    | Los Estudiantes | 900    | 1          | 10    | Viña del Mar | 40             | Don Cristian         | 666666666     | 326            | 1                 | 42537634     | Ernesto         |


## Primera forma normal

Para cumplir con la primera forma normal:

1.  Cada campo debe tener un sólo valor.
    -   La columna vendedor contiene múltiples valores, estos se separan creando un nuevo registro. Esto generará RUTs de vendedores nulos.
2.  No pueden haber grupos repetitivos.
    -   Se crean una tabla para productos, local, bodega, vendedor y venta.
    -   Los valores de cada columna deben ser del mismo tipo. Se modifica la columna existencia (boolean) y nombre vendedor (se eliminan comillas).

**Tabla producto**

| **codigo\_producto** | producto | precio |
|----------------------|----------|--------|
| 43                   | te       | 2000   |
| 25                   | cafe     | 1500   |
| 6                    | leche    | 900    |
| 64                   | queso    | 2000   |


**Tabla local**

| local           | ubicacion    | numero\_bodega | codigo\_producto | existencia | stock |
|-----------------|--------------|----------------|------------------|------------|-------|
| Las Rosas       | Providencia  | 21             | 43               | TRUE       | 30    |
| Los Pinos       | Los Leones   | 35             | 25               | TRUE       | 40    |
| Los Estudiantes | Viña del Mar | 40             | 6                | TRUE       | 10    |
| Las Rosas       | Providencia  | 62             | 64               | TRUE       | 70    |

**Tabla vendedor**

| **\#rut\_vendedor** | nombre\_vendedor |
|---------------------|------------------|
| 33333333            | Juan             | 
| 22222222            | Rosita           | 
| 444444444           | Jorge            | 
| 555555555           | Luisita          |
| 666666666           | Cristian         |


**Tabla venta**

| **\#numero\_boleta** | **\#rut\_vendedor** | *codigo\_producto* | *numero\_bodega* | cantidad\_vendida | rut\_cliente | nombre\_cliente |
|----------------------|---------------------|--------------------|------------------|-------------------|--------------|-----------------|
| 534                  | 33333333            | 43                 | 21               | 1                 | 77777777     | Fernando        |
| 534                  | 22222222            | 43                 | 21               | 1                 | 77777777     | Fernando        |
| 75643                | 444444444           | 25                 | 35               | 1                 | 3332323      | Cristian        |
| 75643                | 555555555           | 25                 | 35               | 1                 | 3332323      | Cristian        |
| 324                  | 666666666           | 6                  | 40               | 1                 | 56434513     | Angela          |
| 537                  | 33333333            | 64                 | 62               | 1                 | 123267845    | Iván            |
| 537                  | 22222222            | 64                 | 62               | 1                 | 123267845    | Iván            |
| 7569                 | 444444444           | 25                 | 35               | 1                 | 4537145      | Felipe          |
| 7569                 | 555555555           | 25                 | 35               | 1                 | 4537145      | Felipe          |
| 326                  | 666666666           | 6                  | 40               | 1                 | 42537634     | Ernesto         |



## Segunda forma normal

1.  Debe satisfacer la 1FN.
2.  Los atributos deben depender de toda la clave primaria, y no solo una parte de ella.
3.  Los atributos que dependan de manera parcial de la clave primaria deben ser eliminados o almacenados en una nueva entidad.
    -   Debemos fijarnos en las tablas que tengan claves primarias compuestas. En este caso, la tabla local se separa en local y bodega.

**Tabla producto**

| **\#codigo\_producto** | producto | precio |
|----------------------|----------|--------|
| 43                   | te       | 2000   |
| 25                   | cafe     | 1500   |
| 6                    | leche    | 900    |
| 64                   | queso    | 2000   |

**Tabla local**

| **\#id\_local** | local           | ubicacion    |
|-----------------|-----------------|--------------|
| 1               | Las Rosas       | Providencia  |
| 2               | Los Pinos       | Los Leones   |
| 3               | Los Estudiantes | Viña del Mar |

**Tabla bodega**

| **\#numero\_bodega** | *id\_local* | codigo\_producto | existencia | stock |
|----------------------|-------------|------------------|------------|-------|
| 21                   | 1           | 43               | TRUE       | 30    |
| 35                   | 2           | 25               | TRUE       | 40    |
| 40                   | 3           | 6                | TRUE       | 10    |
| 62                   | 1           | 64               | TRUE       | 70    |

**Tabla vendedor**

| **\#rut\_vendedor** | nombre\_vendedor |
|---------------------|------------------|
| 33333333            | Juan             | 
| 22222222            | Rosita           | 
| 444444444           | Jorge            | 
| 555555555           | Luisita          |
| 666666666           | Cristian         |


**Tabla venta**

| **\#numero\_boleta** | **\#rut\_vendedor** | *codigo\_producto* | *numero\_bodega* | cantidad\_vendida | rut\_cliente | nombre\_cliente |
|----------------------|---------------------|--------------------|------------------|-------------------|--------------|-----------------|
| 534                  | 33333333            | 43                 | 21               | 1                 | 77777777     | Fernando        |
| 534                  | 22222222            | 43                 | 21               | 1                 | 77777777     | Fernando        |
| 75643                | 444444444           | 25                 | 35               | 1                 | 3332323      | Cristian        |
| 75643                | 555555555           | 25                 | 35               | 1                 | 3332323      | Cristian        |
| 324                  | 666666666           | 6                  | 40               | 1                 | 56434513     | Angela          |
| 537                  | 33333333            | 64                 | 62               | 1                 | 123267845    | Iván            |
| 537                  | 22222222            | 64                 | 62               | 1                 | 123267845    | Iván            |
| 7569                 | 444444444           | 25                 | 35               | 1                 | 4537145      | Felipe          |
| 7569                 | 555555555           | 25                 | 35               | 1                 | 4537145      | Felipe          |
| 326                  | 666666666           | 6                  | 40               | 1                 | 42537634     | Ernesto         |


## Tercera forma normal

1.  Debe satisfacer 2FN.
2.  Toda entidad debe depender directamente de la clave primaria.
    - Si aún quedan atributos que no dependen directamente de la clave primaria, queda llevarlas a una nueva tabla y 
    se puede relacionar a través de una clave foránea con la tabla proveniente. En este caso se crea tabla cliente, 
    a partir de los datos de la tabla venta.

**Tabla producto**

| **\#codigo\_producto** | producto | precio |
|------------------------|----------|--------|
| 43                     | te       | 2000   |
| 25                     | cafe     | 1500   |
| 6                      | leche    | 900    |
| 64                     | queso    | 2000   |


**Tabla local**

| **\#id\_local** | local           | ubicacion    |
|-----------------|-----------------|--------------|
| 1               | Las Rosas       | Providencia  |
| 2               | Los Pinos       | Los Leones   |
| 3               | Los Estudiantes | Viña del Mar |

**Tabla bodega**

| **\#numero\_bodega** | *id\_local* | codigo\_producto | existencia | stock |
|----------------------|-------------|------------------|------------|-------|
| 21                   | 1           | 43               | TRUE       | 30    |
| 35                   | 2           | 25               | TRUE       | 40    |
| 40                   | 3           | 6                | TRUE       | 10    |
| 62                   | 1           | 64               | TRUE       | 70    |

**Tabla vendedor**

| **\#rut\_vendedor** | nombre\_vendedor |
|---------------------|------------------|
| 33333333            | Juan             | 
| 22222222            | Rosita           | 
| 444444444           | Jorge            | 
| 555555555           | Luisita          |
| 666666666           | Cristian         |


**Tabla venta**

| **\#numero\_boleta** | *rut\_vendedor* | *codigo\_producto* | *numero\_bodega* | *rut\_cliente* | cantidad\_vendida |
|----------------------|-----------------|--------------------|------------------|----------------|-------------------|
| 534                  | 33333333        | 43                 | 21               | 77777777       | 1                 |
| 534                  | 22222222        | 43                 | 21               | 77777777       | 1                 |
| 75643                | 444444444       | 25                 | 35               | 3332323        | 1                 |
| 75643                | 555555555       | 25                 | 35               | 3332323        | 1                 |
| 324                  | 666666666       | 6                  | 40               | 56434513       | 1                 |
| 537                  | 33333333        | 64                 | 62               | 123267845      | 1                 |
| 537                  | 22222222        | 64                 | 62               | 123267845      | 1                 |
| 7569                 | 444444444       | 25                 | 35               | 4537145        | 1                 |
| 7569                 | 555555555       | 25                 | 35               | 4537145        | 1                 |
| 326                  | 666666666       | 6                  | 40               | 42537634       | 1                 |


**Tabla cliente**

| **\#rut\_cliente** | nombre\_cliente |
|--------------------|-----------------|
| 77777777           | Fernando        |        
| 3332323            | Cristian        |
| 56434513           | Angela          |
| 123267845          | Iván            |
| 4537145            | Felipe          |
| 42537634           | Ernesto         |