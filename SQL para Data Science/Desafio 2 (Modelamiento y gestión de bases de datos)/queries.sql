Last login: Wed May 27 20:47:47 on ttys000
(base) Alexandras-MacBook-Pro:~ alexandrabobet$ /Applications/Postgres.app/Contents/Versions/12/bin/psql -p5432 "spotify"
psql (12.3)
Type "help" for help.

spotify=# -- canciones que salieron el año 2018
spotify=# SELECT ca.titulo_cancion, al.año
spotify-# FROM cancion ca, album al
spotify-# WHERE ca.album = al.titulo_album AND al.año = '2018';
 titulo_cancion | año  
----------------+------
 God Is a Woman | 2018
 I Like It      | 2018
 Havanna        | 2018
 Train food     | 2018
 One Minute     | 2018
(5 rows)

spotify=# -- albums y nacionalidad de su artista
spotify=# SELECT titulo_album, nacionalidad
spotify-# FROM album
spotify-# INNER JOIN artista 
spotify-# ON artista = nombre_artista;

      titulo_album       |  nacionalidad  
-------------------------+----------------
 Dua Lipa                | Británica
 Yours Truly             | Estadounidense
 My Everything           | Estadounidense
 Dangerous Woman         | Estadounidense
 Sweetener               | Estadounidense
 Invasion of Privacy     | Estadounidense
 Taylor Swift            | Estadounidense
 Fearless                | Estadounidense
 Speak Now               | Estadounidense
 Red                     | Estadounidense
 1989                    | Estadounidense
 Reputation              | Estadounidense
 Camila                  | Cubana
 Thank Me Later          | Canadiense
 Take Care               | Canadiense
 Nothing Was The Same    | Canadiense
 Views                   | Canadiense
 Scorpion                | Canadiense
 Stoney                  | Estadounidense
 Beerbongs & Bentleys    | Estadounidense
 17                      | Estadounidense
 ?                       | Estadounidense
 Skins                   | Estadounidense
 Real (Edicion Colombia) | Colombia
 Real (Special Edition)  | Colombia
 El Negocio              | Colombia
 La Familia              | Colombia
 La Familia B Sides      | Colombia
 Energia                 | Colombia
 Vibras                  | Colombia
 +                       | Britanica
 x                       | Britanica
 %                       | Britanica
(33 rows)

spotify=# -- Número de track, cancion, album, año de lanzamiento y artista donde las canciones deberán estar ordenadas por año de lanzamiento del albúm, album

spotify=# SELECT numero_del_track, titulo_cancion, titulo_album, año, cancion.artista
spotify-# FROM cancion
spotify-# LEFT JOIN album ON titulo_album = album
spotify-# ORDER BY año, album, artista;
 numero_del_track |    titulo_cancion    |     titulo_album     | año  |    artista     
------------------+----------------------+----------------------+------+----------------
                3 | Love Story           | Fearless             | 2008 | Taylor Swift
                4 | The Story of Us      | Speak Now            | 2010 | Taylor Swift
                5 | Own It               | Nothing Was The Same | 2013 | Drake
               12 | Too Much             | Nothing Was The Same | 2013 | Drake
                2 | Blank Space          | 1989                 | 2014 | Taylor Swift
                8 | Bad Blood            | 1989                 | 2014 | Taylor Swift
                2 | Problem              | My Everything        | 2014 | Ariana Grande
                5 | Break Free           | My Everything        | 2014 | Ariana Grande
                5 | Side To Side         | Dangerous Woman      | 2016 | Ariana Grande
                4 | Into You             | Dangerous Woman      | 2016 | Ariana Grande
               11 | Too Young            | Stoney               | 2016 | Post Malone
                1 | Broken Whiskey Glass | Stoney               | 2016 | Post Malone
               12 | One Dance            | Views                | 2016 | Drake
               10 | New Rules            | Dua Lipa             | 2017 | Dua Lipa
                2 | Lost in Your Light   | Dua Lipa             | 2017 | Dua Lipa
                4 | Havanna              | Camila               | 2018 | Camila Cabello
                7 | I Like It            | Invasion of Privacy  | 2018 | Cardi B
                3 | Train food           | Skins                | 2018 | XXXTENTACION
                7 | One Minute           | Skins                | 2018 | XXXTENTACION
                5 | God Is a Woman       | Sweetener            | 2018 | Ariana Grande
(20 rows)

spotify=# 
