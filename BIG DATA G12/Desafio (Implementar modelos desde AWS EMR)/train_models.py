#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#train_models.py

import pandas as pd
import numpy as np
import pickle, datetime, os, glob
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.naive_bayes import BernoulliNB
import warnings
warnings.filterwarnings("ignore")



def preprocesamiento(csvfile, obj):
    """Given a sklearn model class, a partitioned database in train and test,
    train the model, print a classification_report and pickle the trained model.

    :csvfile: el archivo creado con el script anterior
    :obj: vector objetivo en este caso delay_time
    :returns: X and Y recodificados, con el vector objetivo (y) binarizado segun promedio tiempo de demora 
    :returns: X and Y recodificados, con las columnas (x) segun dtype object

    """
    
    df_tmp = pd.read_csv(csvfile, header=None).rename(columns={0:'delivery_id', 1:'delivery_zone', 2:'monthly_app_usage', 3:'subscription_type', 4:'paid_price',
                                                                5:'customer_size', 6:'menu', 7:'delay_time'})

    # Recodifica del vector objetivo delay_time entre 1 y 0, , identificando como 1 aquellos casos donde hubo una demora superior al promedio.
    obj_mean = df_tmp[obj].mean()
    df_tmp[obj] = np.where(df_tmp[obj] > obj_mean, 1, 0)

    # Recodifica en K-1 columnas de aquellas con dtype object, manteniendo la primera categoria como referencia, eliminando el atributo original y usando pd.get_dummies.
    for colname, serie in df_tmp.drop(obj, axis=1).iteritems():
        if serie.dtype == 'object':
            df_tmp = pd.concat([df_tmp, pd.get_dummies(serie, drop_first=True, prefix=colname)], axis=1) 
            df_tmp = df_tmp.drop(columns = colname)
    y = df_tmp[obj]
    X = df_tmp.drop(obj, axis=1)
    return X, y

# generar la division entre las muestras de entrenamiento y de validacion


obj = 'delay_time'
X, y = preprocesamiento('train_delivery_data.csv', obj)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.33, random_state=11238)


# iniciacilización

models = {LogisticRegression(),
          DecisionTreeClassifier(),
          GradientBoostingClassifier(),
          RandomForestClassifier(),
          BernoulliNB(),
                  }


def report_performance(model, X_train, X_test, y_train, y_test, pickle_it=True):
    """Given a sklearn model class, a partitioned database in train and test,
    train the model, print a classification_report and pickle the trained model.

    :model: a sklearn model class
    :X_train: Feat training matrix
    :X_test: Feat testing matrix
    :y_train: Objective vector training
    :y_test: Objective vector testing
    :pickle_it: If true, store model with an specific tag.
    :returns: TODO

    """
    tmp_model_train = model.fit(X_train, y_train)

    if pickle_it is True:
        model_name = str(model.__class__).replace("'>", '').split('.')[-1]
        pickle.dump(tmp_model_train,
                    open(f"./{y_train.name}_{model_name}.pkl", 'wb')
                    )
    print(classification_report(y_test, tmp_model_train.predict(X_test)))
    
    

for i in models:
    print([i])
    print(f'\nModelos de Clasificacion probados para predecir vector objetivo: {obj}')
    print('-'*100)
    report_performance(i, X_train, X_test, y_train, y_test, pickle_it = True)

