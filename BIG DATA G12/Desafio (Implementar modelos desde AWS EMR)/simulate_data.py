#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#simulate_data.py

"""
generar dos archivos csv con las siguientes columnas:

Variable              Codigo

deliverer_id       np.random.choice(range(100), 1)[0]
delivery_zone      np.random.choice(['I', 'II', 'III', 'IV', 'V', 'VI','VII', 'VIII'])
monthly_app_usage  np.random.poisson(15)
subscription_type  np.random.choice(['Free','Prepaid','Monthly','Trimestral', 'Semestral', 'Yearly'], 1,[.30, .20, 10,.15, .20, .05])[0]
paid_price         np.random.normal(25.45, 10)
customer_size      np.random.poisson(2) + 1
menu               np.random.choice(['Asian', 'Indian', 'Italian','Japanese','French', 'Mexican'],1)[0]
delay_time         np.random.normal(10,3.2)

"""

from random import seed
import numpy as np
import csv


def create_random_row():
    deliverer_id = np.random.choice(range(100), 1)[0]
    delivery_zone = np.random.choice(['I', 'II', 'III', 'IV', 'V', 'VI','VII', 'VIII'])
    monthly_app_usage = np.random.poisson(15)
    subscription_type = np.random.choice(['Free','Prepaid','Monthly',
                                          'Trimestral', 'Semestral', 'Yearly'], 1,[.30, .20, 10,
                                                                                   .15, .20, .05])[0]
    paid_price = np.random.normal(25.45, 10)
    customer_size = np.random.poisson(2) + 1
    menu = np.random.choice(['Asian', 'Indian', 'Italian','Japanese','French', 'Mexican'],1)[0]
    delay_time = np.random.normal(10,3.2)
    return [deliverer_id, delivery_zone, monthly_app_usage, subscription_type, paid_price, customer_size, menu, delay_time]

# creamos el csv train_delivery_data, con 1.000 filas y una semilla aleatoria de 11238

with open('train_delivery_data.csv', 'w') as csvfile:
    file = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    np.random.seed(11238)

    for i in range(1000):
        file.writerow(create_random_row())

print("train delivery created")

# creamos el csv test_delivery_data, con 10.000 filas y una semilla aleatoria de 42

with open('test_delivery_data.csv', 'w') as csvfile:
    file = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    np.random.seed(42)

    for i in range(10000):
        file.writerow(create_random_row())

print("test delivery created")

