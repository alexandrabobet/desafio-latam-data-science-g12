#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#predict_model.py

import pandas as pd
import numpy as np
import pickle, datetime, os, glob
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.naive_bayes import BernoulliNB
import warnings
warnings.filterwarnings("ignore")


def preprocesamiento(csvfile, obj):
    """Given the type of csvfile and object, preprocess and binarize the object variable,
    create column headers for all attributes, apply get dummies to columns which have a dtype:object.

    :csvfile: el archivo creado con el script anterior
    :obj: vector objetivo en este caso delay_time
    :returns: X and Y recodificados, con el vector objetivo (y) binarizado segun promedio tiempo de demora 
    :returns: X and Y recodificados, con las columnas (x) segun dtype object

    """
    
    df_tmp = pd.read_csv(csvfile, header=None).rename(columns={0:'delivery_id', 1:'delivery_zone', 2:'monthly_app_usage', 3:'subscription_type', 4:'paid_price',
                                                                5:'customer_size', 6:'menu', 7:'delay_time'})

    # Recodifica del vector objetivo delay_time entre 1 y 0, , identificando como 1 aquellos casos donde hubo una demora superior al promedio.
    obj_mean = df_tmp[obj].mean()
    df_tmp[obj] = np.where(df_tmp[obj] > obj_mean, 1, 0)

    # Recodifica en K-1 columnas de aquellas con dtype object, manteniendo la primera categoria como referencia, eliminando el atributo original y usando pd.get_dummies.
    for colname, serie in df_tmp.drop(obj, axis=1).iteritems():
        if serie.dtype == 'object':
            df_tmp = pd.concat([df_tmp, pd.get_dummies(serie, drop_first=True, prefix=colname)], axis=1) 
            df_tmp = df_tmp.drop(columns = colname)
    y = df_tmp[obj]
    X = df_tmp.drop(obj, axis=1)
    return X, y



def create_crosstab(pickled_model, X_test, y_test, variables):
    """Returns a pd.DataFrame with k-variable defined crosstab and its prediction on hold out test

    :pickled_model: TODO
    :X_test: TODO
    :y_test: TODO
    :variables: TODO
    :returns: TODO

    """
    tmp_training = X_test.copy()
    unpickle_model = pickle.load(open(pickled_model, 'rb'))
    tmp_training[f"{y_test.name}_yhat"] = unpickle_model.predict(X_test)

    if isinstance(variables, list) is True:
        tmp_query = tmp_training.groupby(variables)[f"{y_test.name}_yhat"].mean()
    else:
        raise TypeError('Variables argument must be a list object')

    del tmp_training, unpickle_model
    return tmp_query


# train/test split
obj = 'delay_time'
X, y = preprocesamiento('test_delivery_data.csv', obj)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.33, random_state=11238)


#crear los queries

query_zone = ['delivery_zone_II', 'delivery_zone_III', 'delivery_zone_IV', 'delivery_zone_V', 'delivery_zone_VII','delivery_zone_VIII']
query_id = ['delivery_id']
query_menu = ['menu_French', 'menu_Indian', 'menu_Italian', 'menu_Mexican']
query_sub = ['subscription_type_Prepaid', 'subscription_type_Semestral', 'subscription_type_Trimestral', 'subscription_type_Yearly']


# evaluar cuál es la probabilidad que un pedido se atrase por sobre la media, para cada una de las zonas de envío
predict_zone = create_crosstab("delay_time_RandomForestClassifier.pkl", X_test, y_test, query_zone)
print('\nProbabilidad según zona de envio\n')
print('-'*100)
print(predict_zone)

# evaluar cuál es la probabilidad que un pedido se atrase por sobre la media, para cada uno de los repartidores.
predict_id = create_crosstab("delay_time_RandomForestClassifier.pkl", X_test, y_test, query_id)
print('\nProbabilidad según repartidor\n')
print('-'*100)
print(predict_id)

# evaluar cuál es la probabilidad que un pedido se atrase por sobre la media, para cada uno de los menús
predict_menu = create_crosstab("delay_time_RandomForestClassifier.pkl", X_test, y_test, query_menu)
print('\nProbabilidad según menu\n')
print('-'*100)
print(predict_menu)

# evaluar cuál es la probabilidad que un pedido se atrase por sobre la media, para cada una de las subscripciones
predict_sub = create_crosstab("delay_time_RandomForestClassifier.pkl", X_test, y_test, query_sub)
print('\nProbabilidad según tipo de subscripción\n')
print('-'*100)
print(predict_sub)


# In[ ]:




