#usr/bin/python
#reducer_4_1.py

# importamos las librerías
import sys
from operator import itemgetter
from itertools import groupby

feed_document = sys.stdin

# generamos un identificador de la palabra previa
previous_counter = None

# generamos un contador de la cantidad de palabras
total_word_count = 0

# para cada una de las líneas en el standard input del paso previo
for line_ocurrence in feed_document:
    word, ocurrence = line_ocurrence.split('\t')
# si es que la palabra es distinta a la previa procedemos a contarla
    if word != previous_counter:
    # si la palabra no es la primera
        if previous_counter is not None:
            print(str(total_word_count) + '\t' + previous_counter)
        previous_counter = word
        total_word_count = 0
    total_word_count += int(ocurrence)

print(previous_counter + '\t' + str(total_word_count))