#usr/bin/python 
#reducer_4_2.py

# importamos las librerías
import sys

feed_document = sys.stdin


# generamos un identificador de la palabra previa
group_counter = None

average = 0

average_counter = 0

# para cada una de las líneas en el standard input del paso previo
for line_ocurrence in feed_document:
    group, occurence = line_ocurrence.split('\t')
# si es que la palabra es distinta a la previa procedemos a contarla
    if group != group_counter:
    # si la palabra no es la primera
        if group_counter is not None:
            print("{}\t{}".format(group_counter, average/average_counter))
        average = 0
        average_counter = 0
        group_counter = group
    average += int(occurence)
    average_counter += 1

print("{}\t{}".format(group_counter, average/average_counter))  
