#usr/bin/python
# mapper_4_2.py

# importamos la librería de sistema
import sys

feed_document = sys.stdin

for line_in_document in feed_document:
    split_doc = line_in_document.split(',')
    promedio_lista = []
    for i in split_doc[:50]: 
        promedio_lista.append(int(i))
    print(split_doc[-1:][0].replace('\n', '')+ '\t' + str(sum(promedio_lista)))    