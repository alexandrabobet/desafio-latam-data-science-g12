#usr/bin/python
# mapper_4_3.py

# importamos la librería de sistema
import sys

# Calculamos el límite mínimo de corte para los usuarios con
# por lo menos un 60% de respuestas positivas
percentage = 0.6
total_questions = 50
limit = percentage * total_questions

# Generamos con las funciones map y filter una salida identificando las palabras
# y la suma de todas las respuestas positivas (1) para cada uno de los
# usuarios que superan el límite de corte del 60% de respuestas positivas.
with sys.stdin as file:
    list(map(lambda x: print(x.lower().strip().split(",")[-1] +'\t' + str( sum(int(float(y)) for y in x.split(",")[0:50]))),
                             list(filter(lambda x: str( sum(int(float(y)) for y in x.split(",")[0:50]) ) >= str(int(limit)), file))))