#usr/bin/python
#reducer_4_3.py

# importamos las librerías
import sys
from operator import itemgetter
from itertools import groupby

dic={}
lista = []
separator = "\t"

# leemos el archivo stdin y creamos una lista de directorios
# Cada directorio contendrá las cuentas y las palabras
for line in sys.stdin:
    k, v = line.strip().split(separator)
    dic = {'count':int(v), 'word':k}
    lista.append(dic)

# preordenamos las palabras por word, para luego aplicar un groupby
lista.sort(key=itemgetter('word'), reverse=False)

# imprime títulos
print('word',separator,'ocur',separator,'sum',separator,'mean')
print('-'*40)

# cuenta las ocurrencias, totaliza la cantidad de respuesas positivas, calcula la media,
# e imprime para cada palabras agrupada, las ocurrencias, la suma total de las respuestas
# positivas y la media, separadas por tabulador.
for word, count in groupby(lista, key=itemgetter('word')):
    q,z=0,0
    for d in count:
        q +=1
        z +=d['count']
    
    print(word, separator, q, separator, z, separator, round(z/q,1))
