
SELECT U.country, U.gender, U.age, count(*) AS count
FROM user_profile U JOIN artists A
ON (U.user_mboxsha1=A.usersha)
WHERE A.artist_name = 'metallica' AND ((U.gender != '99999' OR isnotnull(U.gender)) OR (U.age != 99999 OR isnotnull(U.age)))
GROUP BY U.country, U.gender, U.age
ORDER BY count DESC;