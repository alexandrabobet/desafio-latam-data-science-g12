

/* genere la tabla user_profile   */

CREATE EXTERNAL TABLE user_profile(
usersha VARCHAR(225),
gender VARCHAR(10),
age INT,
country VARCHAR(225),
join_date VARCHAR(255))
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ',';


/* genere la tabla artists   */

CREATE EXTERNAL TABLE artists(
usersha VARCHAR(225),
artist_sha VARCHAR(255),
artist_name VARCHAR(255),
plays INT)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ',';




