

/* Muestra correlaciones entre age y play para Metallica, ordenadas en forma descendente, sin considerar datos nulos */


SELECT  U.country, round(corr(U.age, A.plays),4) AS metallica_age_plays_corr
FROM   user_profile U JOIN artists A ON U.usersha=A.usersha
WHERE A.artist_name ='metallica' AND (U.age <> 99999 OR isnotnull(U.age))
GROUP BY U.country
ORDER BY metallica_age_plays_corr DESC;




/* Muestra correlaciones entre age y play para Metallica, ordenadas en forma ascendente, sin considerar datos nulos */


SELECT  U.country, round(corr(U.age, A.plays),4) AS metallica_age_plays_corr
FROM   user_profile U JOIN artists A ON U.usersha=A.usersha
WHERE A.artist_name ='metallica' AND (U.age <> 99999 OR isnotnull(U.age))
GROUP BY U.country
HAVING corr(U.age, A.plays)< 0
ORDER BY metallica_age_plays_corr ASC;