
/* países con los 10 mayores promedios de reproducciones de Metallica       */
/* Si se desea reducir o ampliar la lista basta cambiar 20 por otro numero  */


SELECT  U.country
        , round(avg(A.plays),2)  AS country_avg_plays
FROM user_profile U JOIN artists A ON (U.usersha=A.usersha)
WHERE A.artist_name = 'metallica'
GROUP BY U.country
ORDER BY country_avg_plays DESC
LIMIT 10;




/* países con los 10 menores promedios de reproducciones de Metallica       */
/* Si se desea reducir o ampliar la lista basta cambiar 20 por otro numero  */


SELECT  U.country
        , round(avg(A.plays),2)  AS country_avg_plays
FROM user_profile U JOIN artists A ON (U.usersha1=A.usersha)
WHERE A.artist_name = 'metallica'
GROUP BY U.country
ORDER BY country_avg_plays ASC
LIMIT 10;
