#!/usr/bin/python3.7

# importamos las librerías
import sys

feed_document = sys.stdin

# generamos un identificador de la palabra previa
previous_counter = None

# generamos un contador de la cantidad de tags
tag_id_count = 0

# generamos un contador tipo float del promedio
average = 0.0

# para cada una de las líneas en el standard input del paso previo
for line_ocurrence in feed_document:
    tag_id, relevance = line_ocurrence.replace('\n', '').split('\t')
# si es que el tag es distinto al previo procedemos a contarlo
    if tag_id != previous_counter:
    # si el tag no es el primero
        if previous_counter is not None:
            print(f"{previous_counter}\t{str(average / tag_id_count)}")
        # asignamos al previous counter al nuevo tag
        previous_counter = tag_id
        # reseteamos el contador para la palabra
        tag_id_count = 0
        average = 0
    # contamos la cantida de ocurrencias    
    tag_id_count += 1
    average += float(relevance)

# imprimimos el resultado final
print(previous_counter + '\t' + str(average / tag_id_count))
