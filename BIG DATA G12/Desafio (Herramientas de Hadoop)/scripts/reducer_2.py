#!/usr/bin/python3.7

import sys

mapper_output = sys.stdin
previous_tag = None
rating_count = 0
rating_sum = 0.0

for line in mapper_output:
    user_id, rating = line.replace('\n', '').split('\t')

    if user_id != previous_tag:

        if previous_tag is not None:
            rating_promedio = round((rating_sum / rating_count),1)
            print(f"{previous_tag}\t\t{rating_count}\t\t{rating_promedio}")

        previous_tag = user_id
        rating_count = 0
        rating_sum = 0.0

    rating_count += 1
    rating_sum += float(rating)

rating_promedio = round((rating_sum / rating_count),1)
print(previous_tag + '\t\t' + str(rating_count) + '\t\t' + str(rating_promedio))
