#!/usr/bin/python3.7

import sys

feed_document = sys.stdin
# generamos un identificador del genero
previous_tag = None
# generamos un contador de la cantidad de peliculas
movie_id_count = 0

# para cada una de las líneas en el standard input del paso previo
for line in feed_document:
    num_genres, movies_id = line.replace('\n', '').split('\t')
    # si es que el genero es distinto al previo procedemos a contarlo
    if num_genres != previous_tag:
        # reporte la cantidad de películas que tiene entre 2 y 10 géneros.
        if previous_tag is not None and (int(previous_tag) >=2 and int(previous_tag) <=10):
            print(f"{previous_tag}\t\t{movie_id_count}")
        # asignamos al previous counter los generos
        previous_tag = num_genres
        # reseteamos el contador para la palabra
        movie_id_count = 0
    # contamos la cantida de ocurrencias
    movie_id_count += 1

if (int(previous_tag) >=2 and int(previous_tag) <=10):
    print(previous_tag + '\t\t' + str(movie_id_count))
