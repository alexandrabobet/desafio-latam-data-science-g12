#!/usr/bin/python3.7

import re, sys
# vamos a leer los datos que provengan del standard input del sistema
feed_document = sys.stdin

# para cada una de las líneas en los datos
for line_in_document in feed_document:
    (movie_id, tag_id, relevance) = line_in_document.replace('\n','').split(',')
    # vamos a generar un print con el tag id y su relevancia
    print(f"{tag_id}\t{relevance}")
