#!/usr/bin/python3.7

import re, sys
# vamos a leer los datos que provengan del standard input del sistema
feed_document = sys.stdin

# para cada una de las líneas en los datos
for line in feed_document:
    user_id, movie_id, rating, timestamp = line.replace('\n','').split(',')

    print(f"{user_id}\t{rating}")
