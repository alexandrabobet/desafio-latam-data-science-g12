#!/usr/bin/python3.7

import sys


feed_document = sys.stdin
# generamos un identificador de la pelicula
previous_tag = None

# generamos un contador de la cantidad de ratings
rating_count = 0
# generamos un contador de la suma de los ratings
rating_sum = 0.0

# para cada una de las líneas en el standard input del paso previo
for line in feed_document:
    movie_id, rating = line.replace('\n', '').split('\t')
    # si es que la pelicula es distinto al previo procedemos a contarlo
    if movie_id != previous_tag:
        # si la pelicular no es la primera
        if previous_tag is not None:
            rating_promedio = round((rating_sum / rating_count),1)
            print(f"{previous_tag}\t\t{rating_promedio}")

        previous_tag = movie_id
        rating_count = 0
        rating_sum = 0.0

    rating_count += 1
    rating_sum += float(rating)

rating_promedio = round((rating_sum / rating_count),1)
print(previous_tag + '\t\t' + str(rating_promedio))
