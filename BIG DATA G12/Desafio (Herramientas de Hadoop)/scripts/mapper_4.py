#!/usr/bin/python3.7

import re, sys

# vamos a leer los datos que provengan del standard input del sistema
feed_document = sys.stdin

# para cada una de las líneas en los datos
for line in feed_document:
    #separar y crear los campos para pelicula, nombre, y genero
    movie_id, *name, genres = line.replace('\n','').split(',')
    genres_list = genres.replace('\n','').split('|')
    #si se encuentra el genero
    if genres_list[0] != '(no genres listed)':
        num_genres = str(len(genres_list))
    else: num_genres = str(0)

    print(f"{num_genres}\t{movie_id}")
